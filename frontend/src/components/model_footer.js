import React from 'react'
import { Pagination } from '@material-ui/lab'
import { Row } from 'react-bootstrap'

class ModelFooter extends React.Component {
    render() {
        return (
            <div>
                <Row className="justify-content-md-center">
                    <br />
                    <Pagination count={this.props.pages} onChange={this.props.handlePag} />
                    <br /><br />
                </Row>
                <div>Total Instances: {this.props.total}</div>
                <br /><br /> 
            </div>)
    }
}

export default ModelFooter