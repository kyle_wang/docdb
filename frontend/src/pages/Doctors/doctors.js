import React from 'react'
import DoctorCard from './doctor_card'
import './../Styles/mod_styles.css'
import { CardGroup, Container, Row} from 'react-bootstrap'
import ModelFooter from './../../components/model_footer.js'
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import algoliaImg from './../../components/algolia.svg'
import { searchClient } from './../../components/SearchClient.js'
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Button from '@material-ui/core/Button';
import Popup from './../../components/popup.js';

class Doctors extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            total: 0,
            doctors: [],
            pages: 0,
            page: 1,
            search: '',
            city_filter: null,
            gender_filter: null,
            sortBy: 'name',
            descend: false,
            compare: [],
            popup: false,
        }
    }

    componentDidMount() {
        this.getDoctorsList()
    }

    // sets doctors based on page number
    getDoctorsList() {
        // todo: differentiate filters and search strings better
        let strlst = []
        if(this.state.search) {
            strlst.push(this.state.search)
        }
        if(this.state.gender_filter) {
            strlst.push(this.state.gender_filter)
        }
        if(this.state.city_filter) {
            strlst.push(this.state.city_filter)
        }
        const str = strlst.join()

        // default to name, increasing
        let index = searchClient.initIndex('doctors')

        if(this.state.descend) {
            if(this.state.sortBy === 'name') {
                index = searchClient.initIndex('doctors_desc')
            }
            if(this.state.sortBy === 'city') {
                index = searchClient.initIndex('doctors_city_desc')
            }
            if(this.state.sortBy === 'last_updated') {
                index = searchClient.initIndex('doctors_last_updated_desc')
            }
        }
        else {
            if(this.state.sortBy === 'city') {
                index = searchClient.initIndex('doctors_city_asc')
            }
            if(this.state.sortBy === 'last_updated') {
                index = searchClient.initIndex('doctors_last_updated_asc')
            }
        }

        // algolia zero indexes pages
        const page = this.state.page - 1

        index.search(str, {page: page})
        .then(result => this.setState({doctors: result.hits, total: result.nbHits, pages: result.nbPages, page: result.page + 1}))
    }

    // sorting options
    sortBy(event) {
        this.setState({page: 1, sortBy: event.target.value}, () => this.getDoctorsList())
    }

    // filtering options for city
    filterByCity(event) {
        this.setState({city_filter: event.target.value}, () => this.getDoctorsList())
    }

    // filtering options for gender
    filterByGender(event) {
        this.setState({gender_filter: event.target.value}, () => this.getDoctorsList())
    }

    handleSearch(event) {
        
        this.setState({search: event.target.value, page: 1}, () => this.getDoctorsList())
    }

    // toggle checkbox and reverse results
    handleCheck(e){
        this.setState({
         descend: !this.state.descend
        }, () => this.getDoctorsList())
    }

    // pagination
    handlePag(event, page) {
        this.setState({page: page}, () => this.getDoctorsList())
    }

    toggleCompareItem(item) {
        // if the item is there, remove it
        if (this.state.compare.includes(item)) {
            this.setState({ compare: this.state.compare.filter((comp) => comp !== item) })
        }
        // if the item is not there, add it
        else {
            this.setState({ compare: [...this.state.compare, item] })
        }
    }

    togglePopup(event) {
        if(this.state.compare.length > 0) {
            this.setState({ popup: !this.state.popup })
        }
    }

    render () { 
        const sortOptions = [['name', 'Name'], ['city', 'City'], ['last_updated', 'Last Update']]
        const cityOptions = [['austin', 'Austin'], ['houston', "Houston"], ['san antonio', 'San Antonio'], ['dallas', 'Dallas']]
        const genderOptions = [['male', 'Male'], ['female', 'Female']]

        return ( 
            <div>
                <h1>Doctors</h1>
                {this.state.popup ? <Popup closePopup={this.togglePopup.bind(this)} type='doctor' items={this.state.compare}/> :
                <Container className="fluid">
                <Row className='form'>
                    {/* sorting options */}
                    <FormControl className='formItem'>
                        <InputLabel>
                            Sort By
                        </InputLabel>
                        <Select
                        onChange={this.sortBy.bind(this)}
                        defaultValue={"name"}
                        >
                            {sortOptions.map((option, idx) => <MenuItem key={idx} value={option[0]}>{option[1]}</MenuItem>)}
                        </Select>
                        <FormControlLabel control={<Checkbox name="checked" color="primary" onClick={this.handleCheck.bind(this)}/>} label="Descending" />
                    </FormControl>

                    {/* filter by city */}
                    <FormControl className='formItem'>
                        <InputLabel>
                            City
                        </InputLabel>
                        <Select
                        onChange={this.filterByCity.bind(this)}
                        defaultValue={""}
                        >
                            <MenuItem value="">
                                <em>None</em>
                            </MenuItem>
                            {cityOptions.map((option, idx) => <MenuItem key={idx} value={option[0]}>{option[1]}</MenuItem>)}
                        </Select>
                    </FormControl>

                    {/* filter by gender */}
                    <FormControl className='formItem'>
                        <InputLabel>
                            Gender
                        </InputLabel>
                        <Select
                        onChange={this.filterByGender.bind(this)}
                        defaultValue={""}
                        >
                            <MenuItem value="">
                                <em>None</em>
                            </MenuItem>
                            {genderOptions.map((option, idx) => <MenuItem key={idx} value={option[0]}>{option[1]}</MenuItem>)}
                        </Select>
                    </FormControl>

                    <input className='input' autoComplete="off" placeholder="Search" type="text" value={this.state.search} onChange={this.handleSearch.bind(this)}/>
                    <Button variant="contained" className='compareButton' onClick={this.togglePopup.bind(this)}>Compare</Button>
                    <img className='algolia' src={algoliaImg} alt='algolia icon' />
                </Row>

                <br/><br/>
                    <CardGroup>
                        <Row className="justify-content-md-center">
                        {(
                            this.state.doctors.map((item) => (
                            <DoctorCard key={item["npi_number"]} id={item["npi_number"]} search={this.state.search} compare={true} handleCompare={this.toggleCompareItem.bind(this)} />
                        )))}
                        </Row>
                    </CardGroup>
                    <br/>
                    <ModelFooter pages={this.state.pages} total={this.state.total} handlePag={this.handlePag.bind(this)} />
                </Container>}
            </div>)
    }
}

export default Doctors