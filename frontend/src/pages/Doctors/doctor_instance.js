import React from 'react'
import { CardGroup, Container } from 'react-bootstrap'
import './../Styles/mod_styles.css'
import { mapGender, formatAddress } from './doctor_utils'
import GoogleApiWrapper from './../../components/Map.js'
import AuxCard from './../AuxServices/aux_card.js'
import TransitCard from './../Transit/transit_card.js'
import { searchClient } from './../../components/SearchClient'

const index = searchClient.initIndex('doctors')

class DocInstance extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            doctor: [],
            aux: [],
            image: null,
            transit_id: null,
            aux_id: null
        }
    }

    componentDidMount() {
        const { number } = this.props.match.params

        // grab the correct aux_service and transit
        index.search(number)
            .then(results => this.getConnections(results.hits[0]))

        const req_str = '/api/doctors/' + String(number)
        fetch(req_str)
            .then(res => res.json())
            .then(data => this.setDoctor(data))
    }

    getConnections(res) {
        const aux_id = res.aux_service[0].aux_location[0].aux_service_id
        const transit_id = res.transit_connection[0].id
        this.setState({ transit_id: transit_id, aux_id: aux_id })
    }

    setDoctor(data) {
        fetch(`https://maps.googleapis.com/maps/api/streetview?size=300x300&location=${data.address}&key=AIzaSyA2WpzkLDlb7eLIRQ29imgybOIJ9Vr6zlE`)
            .then(res => this.setState({ image: res["url"] }))
        this.setState({ doctor: data })
    }

    render() {
        const doc = this.state.doctor
        const specialities = String(doc.specialities).split(',')
        const gender = mapGender(doc.gender)
        const address = formatAddress(doc)
        const last_updated = doc.last_updated ? doc.last_updated.split('T')[0] : null
        const lat = doc.doc_location ? parseFloat(doc.doc_location[0].latitude) : null
        const long = doc.doc_location ? parseFloat(doc.doc_location[0].longitude) : null

        const info = [['Specialities: ', specialities[0]], ['Gender: ', gender], ['Phone: ', doc.phone_number], ['Fax: ', doc.fax_number], ['NPI Number: ', doc.npi_number], ['Last Updated: ', last_updated]]
        return doc ?
            <div>
                <div className="Instance">
                    <div className="row" style={{minHeight: '300px'}}>
                        <div className='info'>
                            <h3>{doc.full_name}</h3>
                            <br />
                            {info.map((item, idx) => <h5 key={idx}> {item[0]} {item[1]} </h5>)}
                            <h5>Address: </h5>
                            <Container style={{ marginLeft: '20px' }}>{address.split('\n').map((item, i) => <h5 key={i}>{item}</h5>)}</Container>
                        </div>
                        <GoogleApiWrapper key={doc.full_name} lat={lat} long={long} />
                    </div>
                    <br/><br/><br/>

                    <div className="row">
                        <div className='col'>
                            <h4>Office</h4>
                            <div>
                                <img src={this.state.image} alt={`office of ${doc.full_name}`} />
                            </div>
                        </div>

                        <br/><br/>
                        
                        <div className='col'>
                            <h4>Related Pharmacy and Transit Route</h4>
                            <CardGroup>
                                {this.state.aux_id ?
                                    <AuxCard id={this.state.aux_id} /> : <div>loading</div>}
                                {this.state.transit_id ?
                                    <TransitCard id={this.state.transit_id} /> : <div>loading</div>}
                            </CardGroup>
                        </div>
                    </div>
                    <br/><br/>
                </div>
            </div>

            : <h1>Error, doctor not found</h1>
    }
}
export default DocInstance
