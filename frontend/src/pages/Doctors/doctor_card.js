import React from 'react'
import Card from 'react-bootstrap/Card'
import './../Styles/mod_styles.css'
import { Link } from 'react-router-dom'
import { mapGender } from './doctor_utils'
import Highlighter from "react-highlight-words";
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Loading from './../../components/loading.js'

class DoctorCard extends React.Component {

  constructor(props) {
    super(props)
    this.state = {
      doctor: [],
      loading: true
    }
  }

  componentDidMount() {
    const req_str = '/api/doctors/' + String(this.props.id)
    fetch(req_str)
      .then(res => res.json())
      .then(data => this.setState({ doctor: data, loading: false }))
  }

  compare(event) {
    event.stopPropagation()
    this.props.handleCompare(this.props.id)
  }

  render() {
    const doctor = this.state.doctor
    const full_name = doctor.full_name ? doctor.full_name : ''
    const specialities = String(doctor.specialities).split(',')
    const city = doctor.city ? doctor.city : ''
    const last_updated = doctor.last_updated ? doctor.last_updated.split('T')[0] : ''
    let spec = specialities[0] ? specialities[0] : ''
    const gender = mapGender(doctor.gender)
    const highlights = [spec, gender, city]

    // student string be way too long
    if (spec.split(' ')[0] === "Student") {
      spec = "Student"
    }

    return (
      this.state.loading ? <Card className="InstanceCard"><br/><br/><Loading/></Card> :
      <Card className="InstanceCard" >
        <Card.Body>
          <Link className="Link" to={`/doctors/${doctor.npi_number}`}>
            <Card.Title>
              <Highlighter
                searchWords={[this.props.search]}
                textToHighlight={full_name}
                highlightStyle={{ backgroundColor: "#ffd54f" }}
              />
            </Card.Title>
            <Card.Text>
              {highlights.map((term, idx) => (
                <span key={idx}>
                  <Highlighter searchWords={[this.props.search]} textToHighlight={term} highlightStyle={{ backgroundColor: "#ffd54f" }} />
                  <br />
                </span>)
              )}
                  Last Update:
                  <Highlighter searchWords={[this.props.search]} textToHighlight={' ' + last_updated} highlightStyle={{ backgroundColor: "#ffd54f" }} />
              <br />
            </Card.Text>
          </Link>

          {/* checkbox for comparison */}
          {this.props.compare ?
            <div style={{ width: '80%', position: 'absolute', bottom: '0px' }}>
              <FormControlLabel control={<Checkbox name="checked" color="primary" onClick={this.compare.bind(this)} />} label="Compare" />
            </div> :
            null}
        </Card.Body>
      </Card>)
  }
}

export default DoctorCard