import React from 'react'
import main_image from './images/splash.png'
import { Button } from 'react-bootstrap'
import { Link } from 'react-router-dom'
import './home_styles.css'
import { Redirect } from 'react-router-dom'
import SearchIcon from '@material-ui/icons/Search';

class Home extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            search: '',
            redirect: false
        }
    }

    // switch to the search page when the user hits enter on the search bar
    handleKeyPress(event) {
        if (event.key === 'Enter')
            this.setState({ redirect: true })
    }

    // switch to the search page when the user clicks search button
    handleClick(event) {
        this.setState({ redirect: true })
    }

    // update search query
    handleSearch(event) {
        this.setState({ search: event.target.value })
    }

    render() {
        // redirect only if the state is true
        if (this.state.redirect) {
            return (
                <Redirect to={{
                    pathname: `/search`,
                    state: { search: this.state.search }
                }} />
            )
        }

        return (
            <div className="text-center">
                <h1 className="display">Welcome to MyMedtro</h1>
                {/* bus */}
                <img src={main_image} alt='medtro_bus' className="img-fluid" height="60%" width="60%"/>
                <br>
                </br>

                {/* buttons */}
                <Link className="btn-responsive" to={`/doctors`}>
                    <Button variant="primary">Doctors</Button>{' '}
                </Link>
                <Link className="btn-responsive" to={`/transit`}>
                    <Button variant="primary">Transit</Button>{' '}
                </Link>
                <Link className="btn-responsive" to={`/aux_services`}>
                    <Button variant="primary">Auxillary Services</Button>{' '}
                </Link>

                <br>
                </br>
                {/* search bar */}
                <input className="input_style" autoComplete='off' type="text" value={this.state.search} onChange={this.handleSearch.bind(this)} onKeyPress={this.handleKeyPress.bind(this)} name="search" placeholder="Search" />
                <button type="submit" onClick={this.handleClick.bind(this)}><SearchIcon /></button>
            </div>)
    }
}

export default Home