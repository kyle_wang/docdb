import React from 'react'
import {
    BarChart, Bar, XAxis, YAxis, CartesianGrid, Tooltip, Legend, PieChart, Pie, LineChart, Line, ResponsiveContainer
}
    from 'recharts'
import Loading from './../../components/loading.js'
import '../Styles/vis_styles.css'

class ProviderVisualizations extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            partyCountsPoliticians: [],
            loadingPartyCountsPoliticians: true, 
            runningForCounts: [], 
            loadingRunningForCounts: true, 
            totalVotersByYear: [], 
            loadingTotalVotersByYear: true, 
            partyWinCountsByYear: [], 
            loadingPartyWinCountsByYear: true, 
            partyCountsDistricts: [],
            loadingPartyCountsDistricts: true
        }
    }

    componentDidMount() {
        const req_str = '/api/provider_party_counts_politicians'
        fetch(req_str)
            .then(res => res.json())
            .then(data => this.setState({ partyCountsPoliticians: data, loadingPartyCountsPoliticians: false }))

        const req_str2 = '/api/provider_running_for_counts'
        fetch(req_str2)
            .then(res => res.json())
            .then(data => this.setState({ runningForCounts: data, loadingRunningForCounts: false }))

        const req_str3 = '/api/provider_total_voters_by_year'
        fetch(req_str3)
            .then(res => res.json())
            .then(data => this.setState({ totalVotersByYear: data, loadingTotalVotersByYear: false }))

        const req_str4 = '/api/provider_party_win_counts_by_year'
        fetch(req_str4)
            .then(res => res.json())
            .then(data => this.setState({ partyWinCountsByYear: data, loadingPartyWinCountsByYear: false }))

        const req_str5 = '/api/provider_party_counts_districts'
        fetch(req_str5)
            .then(res => res.json())
            .then(data => this.setState({ partyCountsDistricts: data, loadingPartyCountsDistricts: false }))

    }

    render() {
        return (
            <div>
                <p><i>We are not the fastest counters, so these visualizations will take time to load</i></p>
                <br>
                </br>
                <div>
                    <h2>Number of Politicians Per Party</h2>
                    {this.state.loadingPartyCountsPoliticians ? <Loading /> : (
                        <ResponsiveContainer width="95%" height={500}>
                            <PieChart>
                                <Tooltip />
                                <Pie isAnimationActive={true} data={this.state.partyCountsPoliticians} dataKey='Count' nameKey='Party' cy={200} outerRadius={80} fill="#e63946" label />

                            </PieChart>
                        </ResponsiveContainer>
                    )}
                </div>

                <div>
                    <h2>Different Offices Politicians are Running For</h2>
                    {this.state.loadingRunningForCounts ? <Loading /> : (
                        <BarChart width={600} height={400} data={this.state.runningForCounts}
                            margin={{ top: 50, right: 20, left: 20, bottom: 50 }}>
                            <CartesianGrid strokeDasharray="3 3" />
                            <XAxis dataKey="Office" />
                            <YAxis />
                            <Tooltip />
                            <Bar dataKey="Count" fill="#1d3557" />
                        </BarChart>
                    )}
                </div>

                <div>
                    <h2>Total Voter Turnout Over the Years in Millions</h2>
                    {this.state.loadingTotalVotersByYear ? <Loading /> : (
                        <div styles={{overflow: 'visible'}}>
                        <LineChart width={500} height={300} data={this.state.totalVotersByYear}
                            margin={{ top: 5, right: 30, left: 20, bottom: 5 }}>
                            <XAxis dataKey="Year" />
                            <YAxis />
                            <CartesianGrid strokeDasharray="3 3" />
                            <Tooltip />
                            <Line type="monotone" dataKey="Voters" stroke="#1d3557" activeDot={{ r: 8 }} />
                        </LineChart>
                        </div>
                    )}
                </div>


                
                <div>
                    <h2>Party Win Counts Over the Years</h2>
                    {this.state.loadingPartyWinCountsByYear ? <Loading /> : (
                        <LineChart width={600} height={300} data={this.state.partyWinCountsByYear}
                            margin={{ top: 5, right: 30, left: 20, bottom: 5 }}>
                            <XAxis dataKey="Year" />
                            <YAxis />
                            <CartesianGrid strokeDasharray="3 3" />
                            <Tooltip />
                            <Legend />
                            <Line type="monotone" dataKey="D" stroke="#1d3557" activeDot={{ r: 8 }} />
                            <Line type="monotone" dataKey="R" stroke="#e63946" />
                        </LineChart>
                    )}
                </div>

                <div>
                    <h2>Number of Democrat and Republican Districts</h2>
                    {this.state.loadingPartyCountsDistricts ? <Loading /> : (
                        <ResponsiveContainer width="95%" height={500}>
                            <PieChart>
                                <Tooltip />
                                <Pie isAnimationActive={true} data={this.state.partyCountsDistricts} dataKey='Count' nameKey='Party' outerRadius={80} fill="#e63946" label />

                            </PieChart>
                        </ResponsiveContainer>

                    )}
                </div>


                
            </div>
        )
    }
}

export default ProviderVisualizations