export const formatProducts = (prods) => {
    return JSON.parse(prods)
}

export const formatAddress = (aux) => {
    return aux.address + '\n' + aux.city + ', ' + aux.state + ' ' + aux.zipcode
}
