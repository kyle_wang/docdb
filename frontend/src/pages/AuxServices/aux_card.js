import React from 'react'
import Card from 'react-bootstrap/Card'
import './../Styles/mod_styles.css'
import { Link } from 'react-router-dom'
import { formatProducts } from './aux_utils'
import Highlighter from "react-highlight-words";
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Loading from './../../components/loading.js'

class AuxCard extends React.Component {

  constructor(props) {
    super(props)
    this.state = {
      place: [],
      loading: true
    }
  }

  componentDidMount() {
    const req_str = '/api/aux_services/' + String(this.props.id)
    fetch(req_str)
      .then(res => res.json())
      .then(data => this.setState({ place: data, loading: false }))
  }

  compare(event) {
    event.stopPropagation()
    this.props.handleCompare(this.props.id)
  }

  render() {
    const place = this.state.place
    const products = place.product
    const clean_prods = products ? formatProducts(products) : []
    const city = place.city ? place.city : ''
    const zip = place.zipcode ? place.zipcode : ''
    const state = place.state ? place.state : ''
    const company_name = place.company_name ? place.company_name : ''
    const address = place.address ? place.address : ''

    const highlights = [address, zip, city, state]

    return (
      this.state.loading ? <Card className="InstanceCard"><br/><br/><Loading/></Card> :
      <Card className="InstanceCard">
        {/* <Card.Img variant="top" src='' /> */}
        {/* placeholder for some sort of pic */}
        <Card.Body>
          <Link className="Link" to={`/aux_services/${place.id}`}>
            
            <Card.Title>
              <Highlighter searchWords={[this.props.search]} textToHighlight={company_name} highlightStyle={{ backgroundColor: "#ffd54f" }} />
            </Card.Title>

            <Card.Text>
              {highlights.map((term, idx) => (
                <span key={idx}>
                  <Highlighter searchWords={[this.props.search]} textToHighlight={term} highlightStyle={{ backgroundColor: "#ffd54f" }} />
                  <br />
                </span>)
              )}
              {clean_prods.map((item, idx) =>
                <Highlighter key={idx} searchWords={[this.props.search]} textToHighlight={item + '\n'} highlightStyle={{ backgroundColor: "#ffd54f" }} />
              )}
              <br />
            </Card.Text>
            <br />
            <br />
          </Link>

          {/* checkbox for comparison */}
          {this.props.compare ?
            <div style={{ width: '80%', position: 'absolute', bottom: '0px' }}>
              <FormControlLabel control={<Checkbox name="checked" color="primary" onClick={this.compare.bind(this)} />} label="Compare" />
            </div> :
            null}
        </Card.Body>
      </Card>)
  }
}

export default AuxCard