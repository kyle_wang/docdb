import React from 'react'
import AuxCard from './aux_card'
import { CardGroup, Container, Row } from 'react-bootstrap'
import ModelFooter from './../../components/model_footer.js'
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import { searchClient } from './../../components/SearchClient.js'
import algoliaImg from './../../components/algolia.svg'
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Button from '@material-ui/core/Button';
import Popup from './../../components/popup.js';
import './../Styles/mod_styles.css'

class Places extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            total: 0,
            places: [],
            pages: 0,
            page: 1,
            search: '',
            city_filter: null,
            product_filter: null,
            sortBy: 'company_name',
            descend: false,
            compare: [],
            popup: false,
        }
    }

    componentDidMount() {
        this.getAuxList()
    }

    toggleCompareItem(item) {
        // if the item is there, remove it
        if (this.state.compare.includes(item)) {
            this.setState({ compare: this.state.compare.filter((comp) => comp !== item) })
        }
        // if the item is not there, add it
        else {
            this.setState({ compare: [...this.state.compare, item] })
        }
    }

    togglePopup(event) {
        if(this.state.compare.length > 0) {
            this.setState({ popup: !this.state.popup })
        }
    }

    getAuxList(event) {
        let strlst = []
        if (this.state.search) {
            strlst.push(this.state.search)
        }
        if (this.state.product_filter) {
            strlst.push(this.state.filter)
        }
        if (this.state.city_filter) {
            strlst.push(this.state.city_filter)
        }
        const str = strlst.join()

        let index = searchClient.initIndex('aux_services')

        if (this.state.descend) {
            if (this.state.sortBy === 'company_name') {
                index = searchClient.initIndex('aux_services_desc')
            }
            if (this.state.sortBy === "city") {
                index = searchClient.initIndex('aux_services_city_desc')
            }
        }
        else {
            if (this.state.sortBy === "city") {
                index = searchClient.initIndex('aux_services_city_asc')
            }
        }
        const page = this.state.page - 1

        index.search(str, { page: page })
            .then(result => this.setState({ places: result.hits, total: result.nbHits, pages: result.nbPages, page: result.page + 1 }))
    }

    // sorting options
    sortBy(event) {
        this.setState({ sortBy: event.target.value }, () => this.getAuxList())
    }

    // filtering options by city
    filterByCity(event) {
        this.setState({ city_filter: event.target.value }, () => this.getAuxList())
    }

    // filtering options by product
    filterByProduct(event) {
        this.setState({ product_filter: event.target.value }, () => this.getAuxList())
    }

    // search
    handleSearch(event) {
        this.setState({ search: event.target.value, page: 1 }, () => this.getAuxList())
    }

    // toggle checkbox and reverse results
    handleCheck(event) {
        this.setState({
            descend: !this.state.descend
        }, () => this.getAuxList())
    }

    // pagination
    handlePag(event, page) {
        this.setState({ page: page }, () => this.getAuxList())
    }

    render() {
        const sortOptions = [['company_name', 'Company Name'], ['city', 'City']]
        const cityOptions = [['austin', 'Austin'], ['houston', "Houston"], ['san antonio', 'San Antonio'], ['dallas', 'Dallas']]
        const productOptions = [['blood glucose monitors', 'Blood Glucose Monitors'], ['automatic external defibrillators', 'AEDs']]

        return (
            <div>
                <h1>Auxillary Services</h1>
                {this.state.popup ? <Popup closePopup={this.togglePopup.bind(this)} type='aux' items={this.state.compare}/> :
                    <Container className="fluid">
                        <Row className='form'>
                            {/* sorting options */}
                            <FormControl className='formItem'>
                                <InputLabel>
                                    Sort By
                            </InputLabel>
                                <Select
                                    onChange={this.sortBy.bind(this)}
                                    defaultValue={"company_name"}
                                >
                                    {sortOptions.map((option, idx) => <MenuItem key={idx} value={option[0]}>{option[1]}</MenuItem>)}
                                </Select>
                                <FormControlLabel control={<Checkbox name="checked" color="primary" onClick={this.handleCheck.bind(this)} />} label="Descending" />
                            </FormControl>

                            {/* filter by city */}
                            <FormControl className='formItem'>
                                <InputLabel>
                                    City
                            </InputLabel>
                                <Select
                                    onChange={this.filterByCity.bind(this)}
                                    defaultValue=""
                                >
                                    <MenuItem value="">
                                        <em>None</em>
                                    </MenuItem>
                                    {cityOptions.map((option, idx) => <MenuItem key={idx} value={option[0]}>{option[1]}</MenuItem>)}
                                </Select>
                            </FormControl>

                            {/* filter by product */}
                            <FormControl className='formItem'>
                                <InputLabel>
                                    Product
                            </InputLabel>
                                <Select
                                    onChange={this.filterByProduct.bind(this)}
                                    defaultValue=""
                                >
                                    <MenuItem value="">
                                        <em>None</em>
                                    </MenuItem>
                                    {productOptions.map((option, idx) => <MenuItem key={idx} value={option[0]}>{option[1]}</MenuItem>)}
                                </Select>
                            </FormControl>

                            <input className='input' placeholder="Search" type="text" value={this.state.search} onChange={this.handleSearch.bind(this)} />
                            <Button variant="contained" className='compareButton' onClick={this.togglePopup.bind(this)}>Compare</Button>
                            <img className='algolia' src={algoliaImg} alt='algolia icon' />
                        </Row>
                        <br /><br />

                        <CardGroup>
                            <Row className="justify-content-md-center">
                                {(
                                    (this.state.places).map((item) => (
                                        <AuxCard key={item["id"]} id={item["id"]} search={this.state.search} compare={true} handleCompare={this.toggleCompareItem.bind(this)} />
                                    )))}
                            </Row>
                        </CardGroup>

                        <br />
                        <ModelFooter pages={this.state.pages} total={this.state.total} handlePag={this.handlePag.bind(this)} />
                    </Container>}
            </div>)
    }
}

export default Places