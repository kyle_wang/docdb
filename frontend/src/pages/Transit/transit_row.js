import React from 'react'
import { Redirect } from 'react-router-dom'
import Highlighter from "react-highlight-words";
import { formatTime, getElapsedTime } from './transit_utils.js'
import Checkbox from '@material-ui/core/Checkbox';

class TransitRow extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            redirect: false,
            route: null,
            doc_loc: null,
            aux_loc: null,
        }
    }

    componentDidMount() {
        // grab the coordinates
        const originLat = this.props.route.aux_latitude
        const originLong = this.props.route.aux_longitude
        const destLat = this.props.route.doctor_latitude
        const destLong = this.props.route.doctor_longitude

        // get realtime transit data
        const req_str = 'https://transit.hereapi.com/v8/routes?origin=' + originLat + ',' + originLong + '&mode=pedestrian&apiKey=fEG1fE4oIMTMG6uBiM7gQH7_yACCQ36e60HsCObc7S0&destination=' + destLat + ',' + destLong
        fetch(req_str)
            .then(res => res.json())
            .then(data => Object.keys(data).includes('notices') ? this.setState({ route: null }) : this.setState({ route: data["routes"] }))
    }


    // helps with routing
    onRowClick() {
        this.setState({ redirect: true })
    }

    compare(event) {
        this.props.handleCompare(this.props.id)
    }

    render() {
        const id = this.props.route.id
        const sections = this.state.route ? this.state.route[0] ? this.state.route[0].sections : [] : []

        const destination = this.props.route.doctor_name
        const origin = this.props.route.aux_company_name

        const departure = sections ? (sections[0] ? sections[0].departure : null) : null
        const dep_time = departure ? departure.time : null
        const arrival = sections ? sections.length ? (sections[sections.length - 1] ? sections[sections.length - 1].arrival : null) : null : null
        const ar_time = arrival ? arrival.time : null
        const city = this.props.route.aux_city
        const total = dep_time ? ar_time ? getElapsedTime(ar_time, dep_time) : '-' : '-'

        let modes = []
        sections.map(section => modes.push(section.type))
        modes = Array.from(new Set(modes))

        const highlights = [id.toString(), city, origin, destination]

        // redirect only if the state is true
        if (this.state.redirect) {
            return (
                <Redirect to={{
                    pathname: `/transit/${this.props.route.id}`,
                    state: { route: this.state.route, origin: this.props.route.aux_service, destination: this.props.route.doctor }
                }} />
            )
        }

        return (
            <tr>
                {highlights.map((term, idx) => (
                    <td key={idx} onClick={this.onRowClick.bind(this)}>
                        <Highlighter searchWords={[this.props.search]} textToHighlight={term} highlightStyle={{ backgroundColor: "#ffd54f" }} />
                        <br />
                    </td>)
                )}
                <td onClick={this.onRowClick.bind(this)}>{dep_time ? formatTime(dep_time) : '-'}</td>
                <td onClick={this.onRowClick.bind(this)}>{ar_time ? formatTime(ar_time) : '-'}</td>
                <td onClick={this.onRowClick.bind(this)}>{total}</td>
                <td onClick={this.onRowClick.bind(this)}>{modes.join(', ')}</td>
                <td> {<Checkbox name="checked" color="primary" onClick={this.compare.bind(this)}/>} </td>
            </tr>
        )
    }
}

export default TransitRow