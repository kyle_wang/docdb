import React from 'react'
import './../Styles/transit_styles.css'
import { Card } from 'react-bootstrap'
import { formatTime } from './transit_utils.js'

class RouteCard extends React.Component {
    render() {
        const section = this.props.section
        const agency = section.agency
        const departure = section.departure
        const arrival = section.arrival

        return (
            <Card className="TCard">
                <Card.Body>
                    <Card.Title>Step {this.props.idx + 1}</Card.Title>
                    <Card.Text>
                        <span>Mode: {section.type} </span><br />

                        {agency ? (
                            <span>Agency:
                                { ' ' + agency.name} <br />
                                <a style={{textDecoration: 'underline'}} href={agency.website}>{agency.website}</a> <br />
                            </span>)
                            : null}
                        <br />

                        <span>Departure</span><br />
                        <span>{formatTime(departure.time) + ' '}</span>
                        from
                        {this.props.idx === 0
                            ? <span> Current Location</span>
                            : <span>{' ' + departure.place.name}</span>}
                        <br /><br />

                        <span>Arrival</span><br />
                        <span>{formatTime(arrival.time) + ' '}</span>
                        at
                        {this.props.last
                            ? <span> Destination</span>
                            : <span>{' ' + arrival.place.name}</span>
                        }
                    </Card.Text>
                </Card.Body>
            </Card>)
    }
}

export default RouteCard