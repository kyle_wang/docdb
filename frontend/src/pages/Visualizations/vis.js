import React from 'react'
import {
    BarChart, Bar, XAxis, YAxis, CartesianGrid, Tooltip, Legend, PieChart, Pie, Radar, RadarChart, PolarGrid, PolarAngleAxis, PolarRadiusAxis, ResponsiveContainer
}
    from 'recharts'
import '../Styles/vis_styles.css'
import Loading from './../../components/loading.js'

class Visualizations extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            doctorCityGender: [],
            cvsCityCount: [],
            doctorSpecialities: [],
            loadingDoctors: true,
            loadingCvs: true,
            loadingSpecialities: true
        }
    }

    componentDidMount() {
        const req_str = '/api/doctor_city_gender'
        fetch(req_str)
            .then(res => res.json())
            .then(data => this.setState({ doctorCityGender: data, loadingDoctors: false }))

        const req_str2 = '/api/cvs_count_city'
        fetch(req_str2)
            .then(res => res.json())
            .then(data => this.setState({ cvsCityCount: data, loadingCvs: false }))

        const req_str3 = '/api/doctor_specialities'
        fetch(req_str3)
            .then(res => res.json())
            .then(data => this.setState({ doctorSpecialities: data, loadingSpecialities: false }))

    }


    render() {
        return (
            <div style={{ width: '100%', height: 300 }}>
                <div>
                    <h2>Gender of Doctors by City</h2>
                    {this.state.loadingDoctors ? <Loading /> : (
                        <BarChart width={600} height={400} data={this.state.doctorCityGender}
                            margin={{ top: 50, right: 20, left: 20, bottom: 50 }}>
                            <CartesianGrid strokeDasharray="3 3" />
                            <XAxis dataKey="City" />
                            <YAxis />
                            <Tooltip />
                            <Legend />
                            <Bar dataKey="Male" fill="#e63946" />
                            <Bar dataKey="Female" fill="#1d3557" />
                        </BarChart>
                    )}
                </div>

                <div>
                    <h2>Count of CVS by City</h2>
                    {this.state.loadingCvs ? <Loading /> : (
                        <ResponsiveContainer width="95%" height={500}>
                            <PieChart>
                                <Tooltip />
                                <Pie isAnimationActive={true} data={this.state.cvsCityCount} dataKey='Count' nameKey='City' outerRadius={80} fill="#e63946" label />

                            </PieChart>
                        </ResponsiveContainer>
                    )}
                </div>

                <div>
                    <h2>Count of Doctor Speciality</h2>
                    {this.state.loadingSpecialities ? <Loading /> : (
                        <ResponsiveContainer width="95%" height={500}>
                        <RadarChart outerRadius={100} data={this.state.doctorSpecialities}>
                            <PolarGrid />
                            <PolarAngleAxis dataKey="Speciality" />
                            <PolarRadiusAxis />
                            <Tooltip />
                            <Radar nameKey="Speciality" dataKey="Count" stroke="#e63946" fill="#e63946" fillOpacity={0.6} />
                        </RadarChart>
                        </ResponsiveContainer>
                    )}
                </div>
            </div>
        )
    }
}

export default Visualizations