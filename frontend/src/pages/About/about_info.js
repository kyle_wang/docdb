import divyaImg from './Images/divya.jpg'
import mahaaImg from './Images/mahaa.jpg'
import duncanImg from './Images/duncan.jpg'
import saadImg from './Images/saad.jpg'
import kyleImg from './Images/kyle.jpg'
import gitlab from './Images/gitlab.svg'
import reactImg from './Images/react.png'
import dockerImg from './Images/docker.png'
import matImg from './Images/material-ui.svg'
import reactBootImg from './Images/reactbootstrap.svg'
import flaskImg from './Images/flask.png'
import awsImg from './Images/aws.png'
import postmanImg from './Images/postman.png'
import postImg from './Images/postgress.png'
import gnuImg from './Images/gnu.png'
import npiImg from './Images/npireg.gif'
import mapsImg from './Images/maps-icon.svg'
import medImg from './Images/medicare.png'
import hereImg from './Images/here-logo.svg'
import algoliaImg from './Images/algolia.png'
import jestImg from './Images/jest.png';

export const members = [
    {
        name: 'Divya Manohar',
        role: 'Backend Developer',
        jobs: 'Designed and implemented API',
        pic: divyaImg,
        leader: true,
        bio: 'I’m a fourth year CS major at UT Austin. I’m from Plano, Texas and in my free time I enjoy playing tennis, listening to true crime podcasts, and playing Among Us. I also have two cats and love to play with them!\n',
        commits: 0,
        issues: 0,
        tests: 0,
        linkedin: 'https://www.linkedin.com/in/divya-manohar-20862716a/'

    },
    {
        name: 'Saad Ahmad',
        pic: saadImg,
        leader: false,
        role: 'Backend Developer',
        jobs: 'Worked on deployment and API design and implementation',
        bio: 'Hi, I’m Saad. I’m a fourth year CS major from Plano, Texas. I love going on long drives and discovering new movies to watch. I am horrible at cooking, but am hoping to get better at it.\n',
        commits: 0,
        issues: 0,
        tests: 2,
        linkedin: 'https://www.linkedin.com/in/saad-ahmad-83a49115b/'

    },
    {
        name: 'Mahaa Noorani',
        pic: mahaaImg,
        leader: false,
        role: 'Frontend Developer',
        jobs: 'Built and tested the frontend',
        bio: 'Hi, I’m Mahaa! I’m a fourth year CS major from Carrollton, Texas. In my free time, I love running, board games, and spending time outside in any way possible! I’m also a big football fan and love watching NFL games with my family.\n',
        commits: 0,
        issues: 0,
        tests: 1,
        linkedin: 'https://www.linkedin.com/in/mahaa-n-627035175/'

    },
    {
        name: 'Duncan Huntsinger',
        pic: duncanImg,
        leader: false,
        role: 'Backend Developer',
        jobs: 'Toolchains, deployment, and backend testing',
        bio: 'I’m a third year CS major from Schertz, Texas. I enjoy watching true crime web content, playing Factorio, music, doing Chinese/English language exchange, and watching films. I also have a teacup chihuahua named Pilgor.\n',
        commits: 0,
        issues: 0,
        tests: 27,
        linkedin: 'https://www.linkedin.com/in/dbhuntsinger/'

    },
    { 
        name: 'Kyle Wang',
        role: 'Frontend Developer',
        leader: false,
        jobs: 'Wrote technical document',
        pic: kyleImg,
        bio: 'Hi I’m Kyle, I’m a 4th year CS major at UT. I enjoy working out, playing golf, and playing games during my free time. I also have a dog and like to make tik toks.\n',
        commits: 0,
        issues: 0,
        tests: 7,
        linkedin: 'https://www.linkedin.com/in/kyle-wang-00ba92151/'
    }
]

export const apis = [
    {
        name: 'NPI Registry',
        use: 'Scraped API to store relevant information about doctors in Austin',
        image: npiImg,
        link: 'https://npiregistry.cms.hhs.gov',
    },
    {
        name: 'Google Maps Javascript API',
        use: 'Used to dynamically generate map images',
        image: mapsImg,
        link: 'https://developers.google.com/maps/documentation/javascript/overview'
    },
    {
        name: 'Medicare.gov',
        use: 'Scraped API to store relevant information about auxillary services in Austin',
        image: medImg,
        link: 'https://data.medicare.gov'
    },
    {
        name: 'HERE Developer',
        use: 'Used to dynamically generate accurate transit information',
        image: hereImg,
        link: 'https://developer.here.com/develop/rest-apis'
    }
]

export const tools = [
    {
        name: 'Gitlab',
        link: 'https://gitlab.com',
        image: gitlab,
        descr: 'Git repository and CI/CD pipelines'
    },
    {
        name: 'React',
        link: 'https://reactjs.org',
        image: reactImg,
        descr: 'JavaScript library for front-end'
    },
    {
        name: 'Docker',
        link: 'https://www.docker.com',
        image: dockerImg,
        descr: 'Containerization'
    },
    {
        name: 'Material-UI',
        link: 'https://material-ui.com',
        image: matImg,
        descr: 'Front-end Pagination and other Details'
    },
    {
        name: 'React-Bootstrap',
        link: 'https://react-bootstrap.github.io',
        image: reactBootImg,
        descr: 'Design library for ReactUI'
    },
    {
        name: 'Flask',
        link: 'https://flask.palletsprojects.com',
        image: flaskImg,
        descr: 'Web framework for back-end development'
    },
    {
        name: 'AWS',
        link: 'https://aws.amazon.com',
        image: awsImg,
        descr: 'Hosting'
    },
    {
        name: 'Postman',
        link: 'https://www.postman.com',
        image: postmanImg,
        descr: 'API design and testing'
    },
    {
        name: 'PostgreSQL',
        link: 'https://www.postgresql.org',
        image: postImg,
        descr: 'Database'
    },
    {
        name: 'GNU Make',
        link: 'https://www.gnu.org',
        image: gnuImg,
        descr: 'Makefile'
    },
    {
        name: 'Algolia',
        link: 'https://www.algolia.com/',
        image: algoliaImg,
        descr: 'Searching, Sorting, and Filtering'
    },
    {
        name: 'Jest',
        link: 'https://jestjs.io/',
        image: jestImg,
        descr: 'JavaScript unit testing'
    }
]

