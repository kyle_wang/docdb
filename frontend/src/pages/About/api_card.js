import React from 'react'
import { Container, Card } from 'react-bootstrap'
import './../Styles/about_styles.css'

class APICard extends React.Component {
    render() {
        const api = this.props.api
        return (
            <a className="Link" key={api.name} href={api.link}>
                <Card className="api">
                    <br />
                    <Container className="toolPicContainer">
                        <Card.Img variant="top" src={api.image} className="toolpic" />
                    </Container>
                    <Card.Body>
                        <Card.Title>{api.name}</Card.Title>
                        <Card.Text>
                            {api.use}
                        </Card.Text>
                    </Card.Body>
                </Card>
            </a>)
    }
}

export default APICard