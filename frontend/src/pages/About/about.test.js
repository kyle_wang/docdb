
import React from 'react'
import About from './about'

import { render, configure } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'

describe('About page test suite', () => {
    configure({ adapter: new Adapter() })
    let about = render(<About />)
    it('checks for the presence of gitlab and postman icons', () => {
        expect(about.find('.icons').length).toBe(2)
    })
    it('checks for the presence of used tools', () => {
        expect(about.find('.tools').length).toBe(12)
    })

    it('checks for the presence of used APIs', () => {
        expect(about.find('.api').length).toBe(4)
    })
})