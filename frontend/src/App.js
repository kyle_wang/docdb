import React from 'react';
import './App.css';
import './main.css';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import Navbar from './components/Navbar.js';
import Home from './pages/Home/home.js';
import About from './pages/About/about.js';
import Doctors from './pages/Doctors/doctors.js';
import Places from './pages/AuxServices/aux_services.js';
import Transit from './pages/Transit/transit.js';
import DocInstance from './pages/Doctors/doctor_instance.js';
import AuxInstance from './pages/AuxServices/aux_instance.js';
import TransitInstance from './pages/Transit/transit_instance.js';
import SearchPage from './pages/Home/search.js';
import Visualizations from './pages/Visualizations/vis.js';
import ProviderVisualizations from './pages/Provider Visualizations/prov_vis.js';


const App = () => {
  return (
    <BrowserRouter>
      <div className="App">
        <Navbar/>
        <br></br>
        <Switch>
          <Route exact path="/" component={Home}></Route>
          <Route path="/about" component={About}></Route>
          <Route path="/doctors/:number" component={DocInstance}></Route>
          <Route path="/doctors" component={Doctors}></Route>
          <Route path="/aux_services/:id" component={AuxInstance}></Route>
          <Route path="/aux_services" component={Places}></Route>
          <Route path="/transit/:idx" render={(props) => (
            props.match 
            ? <TransitInstance{...props}/>
            : <div>something went wrong</div>)}/>
          <Route path="/transit" component={Transit}></Route>
          <Route path="/search" render={(props) => (
            props.match 
            ? <SearchPage{...props}/>
            : <div>something went wrong</div>)}/>
          <Route path="/visualizations" component={Visualizations}></Route>
          <Route path="/provider_visualizations" component={ProviderVisualizations}></Route>
        </Switch>
      </div>
    </BrowserRouter>
  );
}

export default App;
