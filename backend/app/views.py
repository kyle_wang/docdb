from . import app, db
from .models.schemas import Doctors, AuxServices, TransitConnections
from .models.query_helper import make_aux_service_query, make_doctor_query, make_list
from .models.about import get_about_info
from .models.visualizations import (
    count_doctors_by_city_and_gender,
    count_cvs_by_city,
    count_doctor_specialities,
    get_party_counts_by_politicians,
    get_running_for_counts,
    get_total_voters_by_year,
    get_party_win_counts_by_year,
    get_party_count_by_district,
)

from flask_restless import APIManager
from flask import render_template, request, jsonify, send_from_directory


"""
Create Api
"""
manager = APIManager(app, flask_sqlalchemy_db=db)
# creating api endpoints
manager.create_api(Doctors)
manager.create_api(AuxServices, results_per_page=10)
manager.create_api(TransitConnections, results_per_page=10)

"""
endpoints:
/api/doctors?page=<int:page_number>
/api/doctors/<int:npi_number>

/api/aux_services?page=<int:page_number>
/api/aux_services/<int:id>

/api/transit_connections?page=<int:page_number>
/api/transit_connections/<int:id>

Sorting doctor Endpoints:
/api/sort_doctors?sorting_val=<string:val> (val == city, last_updated, or full_name)
/api/sort_doctors?sorting_val=<string:val>&descending (reverse order)

Sorting aux_services endpoints:
/api/sort_aux_services?sorting_val=<string:val> (val == city or company_name)
/api/sort_aux_services?sorting_val=<string:val>&descending (reverse order)

"""


@app.route("/api/sort_doctors")
def sort_doctors():
    # get query params
    sorting_val = request.args.get("sorting_val")
    descending = False if request.args.get("descending") == None else True

    # build query
    query = make_doctor_query(sorting_val, descending=descending)

    # all_items is a list of sql objects
    all_items = query.all()

    # build response
    response = {
        "num_results": len(all_items),
        "objects": make_list(all_items, "doc_location"),
    }

    # return response
    return response


@app.route("/api/sort_aux_services")
def sort_aux_services():
    # get query params
    sorting_val = request.args.get("sorting_val")
    descending = False if request.args.get("descending") == None else True

    # build query
    query = make_aux_service_query(sorting_val, descending=descending)

    # all_items is a list of sql objects
    all_items = query.all()

    # build response
    response = {
        "num_results": len(all_items),
        "objects": make_list(all_items, "aux_location"),
    }

    # return response
    return response


@app.route("/api/doctor_city_gender")
def doctor_city_gender():
    return jsonify(count_doctors_by_city_and_gender())


@app.route("/api/cvs_count_city")
def cvs_count_city():
    return jsonify(count_cvs_by_city())


@app.route("/api/doctor_specialities")
def doctor_specialities():
    return jsonify(count_doctor_specialities())


@app.route("/api/provider_party_counts_politicians")
def provider_party_counts_by_politicians():
    return jsonify(get_party_counts_by_politicians())


@app.route("/api/provider_running_for_counts")
def provider_running_for_counts():
    return jsonify(get_running_for_counts())


@app.route("/api/provider_total_voters_by_year")
def provider_total_voters_by_year():
    return jsonify(get_total_voters_by_year())


@app.route("/api/provider_party_win_counts_by_year")
def provider_party_win_counts_by_year():
    return jsonify(get_party_win_counts_by_year())


@app.route("/api/provider_party_counts_districts")
def provider_party_counts_by_district():
    return jsonify(get_party_count_by_district())


@app.route("/favicon.ico")
def favicon():
    return send_from_directory(app.template_folder, "favicon.ico")


@app.route("/", defaults={"path": ""})
@app.route("/<path:path>")
def index(path):
    """
    Rendering the pages
    """
    return render_template("index.html")


@app.route("/api/about")
def load_about():
    """
    About information endpoint
    """
    try:
        gitlab_stats = get_about_info()
    except Exception as e:
        return {"result": str(e)}, 500
    return gitlab_stats, 200
