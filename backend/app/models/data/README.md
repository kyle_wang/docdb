# Folder for Adding Items to database 

*The contents of this folder should not be changed unless you are looking to modify the tables in the Database*

## What each file does: 
```
aux_services.py -> Parses Aux Services data
crud.py -> File to fill the 4 tables 
doctors.py -> File to Get and Parse Doctor data
schemas.py -> Defines the schemas for the different tables
transits.py -> File to get transit data
```