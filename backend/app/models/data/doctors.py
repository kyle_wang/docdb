import requests
import json
import urllib3

"""
File to Get and Parse Doctor data
"""


def get_doctors_by_city(city, limit, skip):
    """
    Function that gets up to 200 doctors by city

    Parameter
    --------
    city : str

    The city to search in

    limit : int

    Number of items

    skip : int

    Number of items to skip

    Returns
    -------

    The response
    """

    requests.packages.urllib3.disable_warnings()
    requests.packages.urllib3.util.ssl_.DEFAULT_CIPHERS += ":HIGH:!DH:!aNULL"
    try:
        requests.packages.urllib3.contrib.pyopenssl.util.ssl_.DEFAULT_CIPHERS += (
            ":HIGH:!DH:!aNULL"
        )
    except AttributeError:
        # no pyopenssl support used / needed / available
        pass

    # pull data with get
    url = "https://npiregistry.cms.hhs.gov/api/"
    querystring = {
        "version": "2.1",
        "state": "TX",
        "city": city.lower(),
        "limit": limit,
        "skip": skip,
    }
    payload = ""
    response = requests.request(
        "GET", url, data=payload, params=querystring, verify=False
    )
    return response


def npi_1(result, values):
    """
    Function to get data for results that have an NPI-1 Enumeration

    Parameters
    ----------
    result : JSON of results

    Values : Dict to add values from the results to

    Returns
    -------

    None :
    Updates Values in place

    """
    # get basic info
    basic = result["basic"]

    # get name attributes
    name = []
    if "name_prefix" in basic:
        name.append(basic["name_prefix"].title())
    if "first_name" in basic:
        name.append(basic["first_name"].title())
    if "middle_name" in basic:
        name.append(basic["middle_name"].title())
    if "last_name" in basic:
        name.append(basic["last_name"].title())
    if "name_suffix" in basic:
        name.append(basic["name_suffix"].title())

    values["full_name"] = " ".join(name)

    # get the preferred name
    if "name" in basic:
        values["name"] = basic["name"].title()

    # build gender
    if "gender" in basic:
        values["gender"] = basic["gender"]
    else:
        values["gender"] = "n/a"

    # get last updated
    if "last_updated" in basic:
        values["last_updated"] = basic["last_updated"]


def npi_2(result, values):
    """
    Function to get data for results that have an NPI-2 Enumeration

    Parameters
    ----------
    result : JSON of results

    Values : Dict to add values from the results to

    Returns
    -------

    None :
    Updates Values in place

    """
    # get basic info
    basic = result["basic"]

    # get the preferred name
    if "name" in basic:
        values["name"] = basic["name"].title()

    # get last updated
    if "last_updated" in basic:
        values["last_updated"] = basic["last_updated"]


def present(response):
    """
    Function to present data from passed in dictionary

    Parameters
    ----------
    response : response object

    Returns
    -------

    A list of dictionaries with values:

    npi_number,
    enumeration_type,
    full_name,
    name,
    gender,
    last_updated,
    street,
    city,
    state,
    zip,
    phone_number,
    fax_number,
    Specialities

    """

    # load results
    count = json.loads(response.text)["result_count"]
    results = json.loads(response.text)["results"]

    doc_data = []

    for result in results:
        values = {}

        # try to get npi number
        try:
            values["npi_number"] = result["number"]
        except:
            print("NPI Number Missing")
            continue

        # get enumeration_type
        values["enumeration_type"] = result["enumeration_type"]

        # fill basic info
        if result["enumeration_type"] == "NPI-1":
            npi_1(result, values)
        else:
            npi_2(result, values)

        # build location
        # type(addresses) = list
        addresses = result["addresses"]
        for address in addresses:
            location = []
            # getting the physical address compared to mailing address
            if address["address_purpose"] == "LOCATION":
                location.append(address["address_1"].title())
                # if there is a unit/ste number
                if address["address_2"] != "":
                    location.append(address["address_2"].title())

                # get street name
                values["street"] = " ".join(location)

                # city
                values["city"] = address["city"].title()

                # state
                values["state"] = address["state"]

                # add the dash between the first 6 nums and the last 4
                if len(address["postal_code"]) > 6:
                    values["zip_code"] = (
                        address["postal_code"][:-4] + "-" + address["postal_code"][-4:]
                    )
                else:
                    values["zip_code"] = address["postal_code"]

                # add phone number
                try:
                    phone_number = address["telephone_number"]
                except:
                    phone_number = "Not Found"

                values["phone_number"] = phone_number

                # fax number
                try:
                    fax_number = address["fax_number"]
                except:
                    fax_number = "Not Found"

                values["fax_number"] = fax_number
            else:
                continue

        # build taxonomies
        values["specialities"] = ", ".join(
            [taxonomy["desc"] for taxonomy in result["taxonomies"]]
        ).strip()

        doc_data.append(values)

    return count, doc_data


def get(city, limit=10, skip=0):
    response = get_doctors_by_city(city, limit, skip)
    count, doctors = present(response)
    return count, doctors
