import requests
from sqlalchemy import Column, Integer, String
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()


class TransitConnections(Base):
    __tablename__ = "transit_connections"
    id = Column(Integer, primary_key=True, autoincrement=True)
    aux_service_id = Column(Integer)
    doctor_id = Column(Integer)
    aux_latitude = Column(String)
    aux_longitude = Column(String)
    doctor_latitude = Column(String)
    doctor_longitude = Column(String)
    doctor_city = Column(String)
    aux_city = Column(String)


def clean_doctor(doc):
    # fix gender
    doc["gender"] = "Male" if doc["gender"] == "M" else "Female"

    # fix specials
    specials = set([string.strip() for string in doc["specialities"].split(",")])
    doc["specialities"] = ", ".join(specials)


def get_aux_service(aux_id, endpoint):
    # use my api
    aux_response = requests.request("GET", endpoint + "aux_services/{}".format(aux_id))
    return aux_response.json()


def get_doctor(doc_id, endpoint):
    # use my api
    doc_response = requests.request("GET", endpoint + "doctors/{}".format(doc_id))
    doc_response = doc_response.json()
    # clean doctor
    clean_doctor(doc_response)
    return doc_response
