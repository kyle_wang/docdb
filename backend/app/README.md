# Backend Models and Views

These are the endpoints for the api:

```
/api/doctors?page=<int:page_number>
/api/doctors/<int:npi_number>

/api/aux_services?page=<int:page_number>
/api/aux_services/<int:id>

/api/transit_connections?page=<int:page_number>
/api/transit_connections/<int:id>

/api/sort_doctors?sorting_val=<string:val> (val == city, last_updated, or full_name)
/api/sort_doctors?sorting_val=<string:val>&descending (reverse order)

/api/sort_aux_services?sorting_val=<string:val> (val == city or company_name)
/api/sort_aux_services?sorting_val=<string:val>&descending (reverse order)

/api/doctor_city_gender
/api/cvs_count_city
/api/doctor_specialities

/api/provider_party_counts_politicians
/api/provider_running_for_counts
/api/provider_total_voters_by_year
/api/provider_party_win_counts_by_year
/api/provider_party_counts_districts
```